# @atlaskit/multi-entry-tools

## 0.0.2
### Patch Changes

- [patch] [688f2957ca](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/688f2957ca):

  Fixes various TypeScript errors which were previously failing silently
