# @atlaskit/right-side-panel

## 0.1.4

### Patch Changes

- [patch][f34776be97](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/f34776be97):

  Type definition files are now referenced in package.json

## 0.1.3

### Patch Changes

- [patch][29a1f158c1](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/29a1f158c1):

  Use default react import in typescript files.

## 0.1.2

### Patch Changes

- [patch][06940860ea](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/06940860ea):

  Added unit test

## 0.1.1

### Patch Changes

- [patch][a19a3616a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a19a3616a6):

  Update examples
