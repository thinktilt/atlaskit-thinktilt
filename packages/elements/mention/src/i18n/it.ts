/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Italian
export default {
  'fabric.mention.unknow.user.error': 'Utente {id} sconosciuto',
  'fabric.mention.noAccess.warning':
    'A {name} non verrà inviata una notifica poiché non ha accesso',
  'fabric.mention.noAccess.label': 'Nessun accesso',
  'fabric.mention.error.defaultHeadline': 'Si è verificato un errore',
  'fabric.mention.error.defaultAction': 'Riprova tra qualche secondo',
  'fabric.mention.error.loginAgain':
    'Prova a uscire quindi ad accedere di nuovo',
  'fabric.mention.error.differentText': 'Prova a inserire un testo diverso',
  'fabric.elements.mentions.team.member.count':
    '{0, plural, one{1 membro} other{{0} membri}}',
  'fabric.elements.mentions.team.member.count.including.you':
    '{0, plural, one{{0} membri, incluso te} other{{0} membri, incluso te}}',
  'fabric.elements.mentions.team.member.50plus': 'Più di 50 membri',
  'fabric.elements.mentions.team.member.50plus.including.you':
    'Più di 50 membri, incluso te',
};
