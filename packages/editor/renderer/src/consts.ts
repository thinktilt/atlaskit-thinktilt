const clPrefix = 'ak-renderer-';

export const RendererCssClassName = {
  DOCUMENT: `${clPrefix}document`,
  EXTENSION: `${clPrefix}extension`,
  EXTENSION_OVERFLOW_CONTAINER: `${clPrefix}extension-overflow-container`,
  NUMBER_COLUMN: `${clPrefix}table-number-column`,
};
