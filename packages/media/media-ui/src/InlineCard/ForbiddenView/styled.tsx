import styled from 'styled-components';
import { colors } from '@atlaskit/theme';

export const ForbiddenWrapper = styled.span`
  color: ${colors.N500}
  hyphens: auto;
  overflow-wrap: break-word;
`;
