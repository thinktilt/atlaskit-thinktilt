export const ACTION_PENDING = 'pending';
export const ACTION_LOADING = 'loading';
export const ACTION_RESOLVING = 'resolving';
export const ACTION_RESOLVED = 'resolved';
export const ACTION_UNAUTHORIZED = 'unauthorized';
export const ACTION_RELOAD = 'reloading';
export const ACTION_ERROR = 'errored';

export const MAX_LOADING_DELAY = 1200;
export const MAX_RELOAD_DELAY = 15 * 1000;
